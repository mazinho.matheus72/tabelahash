#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 100

typedef struct HashNode
{
    char *chave;
    int value;
    struct HashNode *next;
} HashNode;

HashNode *createNode(char *chave, int value)
{
    HashNode *node = (HashNode *)malloc(sizeof(HashNode));
    node->chave = chave;
    node->value = value;
    node->next = NULL;
    return node;
}

int hashFunction(char *chave)
{
    int hashValue = 0;
    for (int i = 0; i < strlen(chave); i++)
    {
        hashValue += (int)chave[i];
    }
    return hashValue % SIZE;
}

void inserir(HashNode *hashtable[], char *chave, int value)
{
    int index = hashFunction(chave);
    HashNode *node = createNode(chave, value);
    if (hashtable[index] == NULL)
    {
        hashtable[index] = node;
    }
    else
    {
        HashNode *current = hashtable[index];
        while (current->next != NULL)
        {
            current = current->next;
        }
        current->next = node;
    }
}

int search(HashNode *hashtable[], char *chave)
{
    int index = hashFunction(chave);
    HashNode *current = hashtable[index];
    while (current != NULL)
    {
        if (strcmp(current->chave, chave) == 0)
        {
            return current->value;
        }
        current = current->next;
    }
    return -1;
}

void removeNode(HashNode *hashtable[], char *chave)
{
    int index = hashFunction(chave);
    HashNode *current = hashtable[index];
    HashNode *previous = NULL;
    while (current != NULL)
    {
        if (strcmp(current->chave, chave) == 0)
        {
            if (previous == NULL)
            {
                hashtable[index] = current->next;
            }
            else
            {
                previous->next = current->next;
            }
            free(current);
            return;
        }
        previous = current;
        current = current->next;
    }
}

void printHashtable(HashNode *hashtable[])
{
    for (int i = 0; i < SIZE; i++)
    {
        HashNode *current = hashtable[i];
        printf("%d: ", i);
        while (current != NULL)
        {
            printf("(%s, %d) ", current->chave, current->value);
            current = current->next;
        }
        printf("\n");
    }
}

int main()
{
    HashNode *hashtable[SIZE];
    memset(hashtable, 0, sizeof(hashtable));
    inserir(hashtable, "Maça", 1);
    inserir(hashtable, "Banana", 2);
    inserir(hashtable, "Melão", 3);
    printf("Tabela Hash depois da inserção:\n");
    printHashtable(hashtable);
    printf("Valor da chave'banana': %d\n", search(hashtable, "banana"));
    removeNode(hashtable, "banana");
    printf("Tabela Hash depois da remoção:\n");
    printHashtable(hashtable);
    return 0;
}
